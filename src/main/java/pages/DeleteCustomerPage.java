package pages;

import static org.junit.Assert.assertEquals;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import common.ConfigUrl;

public class DeleteCustomerPage extends ConfigUrl{
	private WebDriverWait wait = new WebDriverWait(driver, 5);
	private String goBackLink = "//*[@id=\"report-success\"]/p/a[2]";
	private String txtSearch = "customerName";
	private String fieldName = "/html/body/div[2]/div[2]/div[1]/div[2]/form/div[2]/table/tbody/tr/td[contains(text(), 'Teste Sicredi')]";
	private String checkAction = "/html/body/div[2]/div[2]/div[1]/div[2]/form/div[2]/table/tbody/tr[1]/td[1]/input";
	private String btnDelete = "text-danger";
	private String txtMsgDelete = "*//div[2]/div[2]/div[3]/div/div/div[2]/p[2]";
	private String labelMsg = "Are you sure that you want to delete this 1 item?";
	private String btnDeleteMsg = "*//div[2]/div[2]/div[3]/div/div/div[3]/button[2]";
	private String msgConfirmDelete = "/html/body/div[3]/span[3]/p";
	private String labelConfirmDelete = "Your data has been successfully deleted from the database.";
	
	public DeleteCustomerPage(WebDriver driver) {
		super(driver);
	}
	
	public void gobackHomepage() {
		driver.findElement(By.xpath(goBackLink)).click();		
	}
		
	public void searchCustomer() throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(txtSearch)));
		WebElement inputsearch = driver.findElement(By.name(txtSearch));
		inputsearch.sendKeys("Teste Sicredi");
		inputsearch.sendKeys(Keys.ENTER);
	}
	
	public void deleteCustomer() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(fieldName)));
		driver.findElement(By.xpath(checkAction)).click();		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(btnDelete)));		
		driver.findElement(By.className(btnDelete)).click();
	}

	public void confirmDelete() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(txtMsgDelete)));
		String sucess = driver.findElement(By.xpath(txtMsgDelete)).getText();
		assertEquals(labelMsg, sucess);
		driver.findElement(By.xpath(btnDeleteMsg)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(msgConfirmDelete)));
		String sucess2 = driver.findElement(By.xpath(msgConfirmDelete)).getText();
		assertEquals(labelConfirmDelete, sucess2);		
	}
	
	public void quitDriver() {
		driver.quit();
	}
}
