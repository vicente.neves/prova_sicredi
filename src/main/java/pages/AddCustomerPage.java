package pages;

import static org.junit.Assert.assertEquals;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import common.ConfigUrl;

public class AddCustomerPage extends ConfigUrl{
	private WebDriverWait wait = new WebDriverWait(driver, 5);
	private String selectVersion = "switch-version-select";
	private String optionVersion = "Bootstrap V4 Theme";
	private String btnCustomer = "#gcrud-search-form > div.header-tools > div.floatL.t5 > a";
	private String txtName = "field-customerName";
	private String txtLastName = "field-contactLastName";
	private String txtFirstName = "field-contactFirstName";
	private String txtPhone = "field-phone";
	private String txtAdress1 = "field-addressLine1";
	private String txtAdress2 = "field-addressLine2";
	private String txtCity = "field-city";
	private String txtState = "field-state";
	private String txtPostalCode = "field-postalCode";
	private String txtCountry = "field-country";
	private String selectEmployeer = "*//div[2]/div/div/div/div[2]/form/div[11]/div/div/a";
	private String searchEmployeer = "#field_salesRepEmployeeNumber_chosen > div > div > input[type=text]";
	private String optionEmployeer = "//*[@id=\"field_salesRepEmployeeNumber_chosen\"]/div/ul/li";
	private String txtCreditLimit = "field-creditLimit";
	private String btnSave = "form-button-save";
	private String xpathTextSucess = "//div[2]/div/div/div/div[2]/form/div[14]/div[2]/p";
	private String labelSucess = "Your data has been successfully stored into the database. Edit Customer or Go back to list";
		
	
	public AddCustomerPage(WebDriver driver) {
		super(driver);
	}
	
	public void selectVersion() {
		Select select = new Select(driver.findElement(By.id(selectVersion)));
		select.selectByVisibleText(optionVersion);
	}
	
	public void addCustomer() {
		WebElement element =  driver.findElement(By.cssSelector(btnCustomer));
		element.click();
	}
	
	public void registerCustomer() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(btnSave)));
		driver.findElement(By.id(txtName)).click();
		driver.findElement(By.id(txtName)).sendKeys("Teste Sicredi");		
		driver.findElement(By.id(txtLastName)).sendKeys("Teste");		
		driver.findElement(By.id(txtFirstName)).sendKeys("Vicente");		
		driver.findElement(By.id(txtPhone)).sendKeys("51 9999-9999");		
		driver.findElement(By.id(txtAdress1)).sendKeys("Av Assis Brasil, 3970");		
		driver.findElement(By.id(txtAdress2)).sendKeys("Torre D");		
		driver.findElement(By.id(txtCity)).sendKeys("Porto Alegre");		
		driver.findElement(By.id(txtState)).sendKeys("RS");		
		driver.findElement(By.id(txtPostalCode)).sendKeys("91000-000");		
		driver.findElement(By.id(txtCountry)).sendKeys("Brasil");		
		driver.findElement(By.xpath(selectEmployeer)).click();
		driver.findElement(By.cssSelector(searchEmployeer)).sendKeys("Fixter");
		driver.findElement(By.xpath(optionEmployeer)).click();		
		driver.findElement(By.id(txtCreditLimit)).sendKeys("200");		
		driver.findElement(By.id(btnSave)).click();
	}

	public void validateMessage() {
		WebDriverWait waitSucess = new WebDriverWait(driver, 5);
		waitSucess.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathTextSucess)));
		String sucess = driver.findElement(By.xpath(xpathTextSucess)).getText();
		assertEquals(labelSucess, sucess);
	}
	
	public void closeBrowser() {
		driver.close();
	}
	
}
