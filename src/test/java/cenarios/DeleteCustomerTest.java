package cenarios;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import common.ConfigUrl;
import pages.DeleteCustomerPage;
import pages.AddCustomerPage;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DeleteCustomerTest {
	private static ConfigUrl config=null;
	private static DeleteCustomerPage delCustomer=null;
	private static AddCustomerPage precondicao = null;
	
	@BeforeClass
	public static void loadObject() throws Exception {
		config = new ConfigUrl();
		delCustomer = new DeleteCustomerPage(config.getDriver());
		precondicao = new AddCustomerPage(config.getDriver());		
	}
	
	@Test
	public void a_StepsDesafio1() {
		precondicao.selectVersion();
		precondicao.addCustomer();
		precondicao.registerCustomer();
		precondicao.validateMessage();
	}	
	
	@Test
	public void a_acessHomepage() {
		delCustomer.gobackHomepage();
	}
	
	@Test
	public void b_searchName() throws InterruptedException {
		delCustomer.searchCustomer();
	}
	
	@Test
	public void c_deleteRegistry() {
		delCustomer.deleteCustomer();
	}
	
	@Test
	public void d_confirmMsg() {
		delCustomer.confirmDelete();
	}
	
	@AfterClass
	public static void stopDriver() {
		delCustomer.quitDriver();
	}
}
