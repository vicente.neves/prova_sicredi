package cenarios;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import common.ConfigUrl;
import pages.AddCustomerPage;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AddCustomerTest {
	private static ConfigUrl config=null;
	private static AddCustomerPage addCustomer=null;
	
@BeforeClass
public static void loadObject() throws Exception {
	config = new ConfigUrl();
    addCustomer = new AddCustomerPage(config.getDriver());
}
	
@Test
public void a_alterVersion() {
	addCustomer.selectVersion();
}

@Test
public void b_acessForm() {
	addCustomer.addCustomer();
}

@Test
public void c_insertFormTxt() {
	addCustomer.registerCustomer();
}

@Test
public void d_validMessage(){
	addCustomer.validateMessage();
}

@AfterClass
public static void exitBrowser() {
	addCustomer.closeBrowser();
}
}
